var cartApp = angular.module ("cartApp", []);

cartApp.controller("cartCtrl", function($scope, $http){
	var contextPath = '/emusicstore';
		
    $scope.refreshCart = function(){
        var url = contextPath + '/rest/cart/' + $scope.cartId;
        $http.get(url).then(function (resp) {
            // alert('get data success.');
            $scope.cart = resp.data;
        }, function(){
            alert('get data failed.');
        });

        /*$http.get('/rest/cart/' + $scope.cartId).success(function (data){
           $scope.cart = data;
       });*/
    };

    $scope.clearCart = function(){
        $http.delete(contextPath + '/rest/cart/' + $scope.cartId).then(function() {
            $scope.refreshCart()
        });

        /*$http.delete('/rest/cart/' + $scope.cartId).success($scope.refreshCart());*/
    };

    $scope.initCartId = function(cartId){
        $scope.cartId = cartId;
        $scope.refreshCart(cartId);
    };

    $scope.addToCart = function(productId){
        $http.put(contextPath + '/rest/cart/add/' + productId).then(function() {
            alert('Product successfully added to the cart!');
        });
        // angular-1.6.7.min.js:125 TypeError: $http.put(...).success is not a function
        /*
        $http.put('/rest/cart/add/' + productId).success(function (){
            alert('Product successfully added to the cart!');
        });*/
    };

    $scope.removeFromCart = function(productId){
        $http.put(contextPath + '/rest/cart/remove/' + productId).then(function() {
            $scope.refreshCart();
        });

        /*$http.put('/rest/cart/remove/' + productId).success(function(data){
           $scope.refreshCart();
        });*/
    };

    $scope.calGrandTotal = function(){
        var grandTotal = 0;
        if (typeof($scope.cart) !== 'undefined'){
            for (var i = 0; i < $scope.cart.cartItems.length; i++){
                grandTotal += $scope.cart.cartItems[i].totalPrice;
            }
            return grandTotal;
        }
        return grandTotal;
    }
});